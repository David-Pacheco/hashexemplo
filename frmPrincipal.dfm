object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Exempo de Hash'
  ClientHeight = 201
  ClientWidth = 447
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 32
    Top = 128
    Width = 66
    Height = 13
    Caption = 'Caracteres: 0'
  end
  object Button1: TButton
    Left = 32
    Top = 88
    Width = 75
    Height = 25
    Caption = 'Hash MD5'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Edit1: TEdit
    Left = 32
    Top = 8
    Width = 289
    Height = 21
    TabOrder = 1
    TextHint = 'Digite o texto a ser Criptografado'
  end
  object Edit2: TEdit
    Left = 32
    Top = 48
    Width = 289
    Height = 21
    ReadOnly = True
    TabOrder = 2
  end
  object Button2: TButton
    Left = 246
    Top = 88
    Width = 75
    Height = 25
    Caption = 'Hash SHA2'
    TabOrder = 3
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 137
    Top = 88
    Width = 75
    Height = 25
    Caption = 'Hash SHA1'
    TabOrder = 4
    OnClick = Button3Click
  end
end
